﻿using OpenQA.Selenium;
using System;

namespace QudiniAutomation.Extensions
{
    public static class WebElementExtensions
    {

        public static string GetLinkText(this IWebElement element)
        {
            return element.Text;
        }

        public static void AssertElementPresent(this IWebElement element)
        {
            if (!IsElementPresent(element))
                throw new Exception(string.Format("Element Not Present exception"));
        }

        public static bool IsElementPresent(this IWebElement element)
        {
            try
            {
                bool ele = element.Displayed;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}

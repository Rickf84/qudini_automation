﻿Feature: Main
Main feature file for the projects tests 

Background: I have a valid token
	Given I am on the URL with valid token
	Then The Landing page is displayed

@mytag
Scenario: Selecting English on landing page
	Given the user is on the landing page
	When they tap the language dropdown
	And select English
	Then User is shown the English language
	
Scenario: Selecting Korean on landing page
	Given the user is on the landing page
	When they tap the language dropdown
	And select Korean
	Then User is shown the Korean language

Scenario: Select Sales
	Given user selects Sales
	Then Sales is shown

Scenario: Select Repairs
	Given user selects Repairs
	Then Repairs is shown

Scenario: Enter valid details
	Given user is on enter details page
	And enters valid details

Scenario: Details page - Name Max Validation
	Given the user tries to enter too many characters in first & last name fields 
	Then only the valid amount are input 

Scenario: Details page name includes numbers and special characters
	Given the user has entered numbers & special characters
	Then then please enter your name is displayed

#Scenario: Invalid phone numbers
#	Given the user has not met number validation
#	Then TBC
#
# #Potentially change to table for different numbers to be used
#Scenario: Country code - google validation
#	Given the user has entered Korean country code
#	Then only korean number is accepted
#
#Scenario: Date validation
#	Given the user has not met DoB validation
#	Then TBC
#
#Scenario: Email validation
#	Given the user has not met email validation
#	Then TBC

Scenario: Details page blank fields
	Given the user hasn't filled out mandatory fields 
	Then tapping next button shows inline errors

Scenario: Kakao returning user Flow
	Given User has already accepted kakao terms
	And User fills out remaining details page
	Then User is added to queue

Scenario: App locked down
	Given user tries to access pages outside of correct flow
	Then user is shown the error page





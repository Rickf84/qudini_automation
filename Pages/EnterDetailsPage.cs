﻿using OpenQA.Selenium;
using QudiniAutomation.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace QudiniAutomation.Pages
{
    class EnterDetailsPage : BasePage
    {
        public EnterDetailsPage(ParallelConfig parallelConfig) : base(parallelConfig) { }

        #region UI Elements

        // Enter details Page
        private IWebElement FirstNameField => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[2]/div[1]/div/input"));
        private IWebElement LastNameField => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[2]/div[2]/div/input"));
        private IWebElement DoBYear => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[5]/div[1]/div/input"));
        private IWebElement DoBMonth => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[5]/div[2]/div/input"));
        private IWebElement DoBDay => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[5]/div[3]/div/input"));
        //private IWebElement AreaCode => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div[2]/div[5]/div[1]/div/div"));
        //private IWebElement SelectAreaCode => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div[3]/ul/li[7]"));
        private IWebElement PhoneNumberInput => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[3]/div/input"));
        private IWebElement KakaoPhoneNumberInput => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[3]/div/div/input"));
        private IWebElement EmailInput => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[6]/div/input"));
        private IWebElement NextBtn => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/button"));
        private IWebElement Checkbox => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[7]/div[1]/label/span"));
        private IWebElement AuthenticateBtn => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[3]/button"));
        //Kakao
        private IWebElement KakaoEmail => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='id_email_2']"));
        private IWebElement KakaoPassword => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='id_password_3']"));
        private IWebElement KakaoBtn => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='login-form']/fieldset/div[8]/button[1]"));

        #endregion

        #region Actions

        public void EnterFirstName()
        {
            Thread.Sleep(1000);
            FirstNameField.SendKeys("Automation");
        }
        public void EnterLastName()
        {
            Thread.Sleep(500);
            LastNameField.SendKeys("Testing");
        }

        public void EnterLongFirstName()
        {
            Thread.Sleep(500);
            FirstNameField.SendKeys("LoremipsumdolorsitametconsecteturadipiscingelitSuspendissevelco");
        }

        public void EnterLongLastName()
        {
            Thread.Sleep(500);
            LastNameField.SendKeys("LoremipsumdolorsitametconsecteturadipiscingelitSuspendissevelco");
        }

        public void EnterInvalidFirstName()
        {
            Thread.Sleep(500);
            FirstNameField.SendKeys("by76%gk:");
            Thread.Sleep(2000);
        }

        public void EnterInvalidLastName()
        {
            Thread.Sleep(500);
            LastNameField.SendKeys("by76%gk:");
            Thread.Sleep(2000);
        }

        public void EnterDoB()
        {
            Thread.Sleep(1000);
            DoBYear.Click();
            DoBYear.SendKeys("1990");
            DoBMonth.SendKeys("12");
            DoBDay.SendKeys("28");
            Thread.Sleep(500);
        }
        public void EnterPhoneNumber()
        {
            Thread.Sleep(500);
            //AreaCode.Click();
            //SelectAreaCode.Click();
            PhoneNumberInput.Click();
            PhoneNumberInput.SendKeys("+44789123456");
            Thread.Sleep(1000);
        }
        public void EnterKakaoPhoneNumber()
        {
            Thread.Sleep(500);
            KakaoPhoneNumberInput.Click();
            KakaoPhoneNumberInput.SendKeys("789123456");
            Thread.Sleep(1000);
        }
        public void EnterEmail()
        {
            Thread.Sleep(1000);
            EmailInput.SendKeys("a@a.aa");
        }

        public void CheckTerms()
        {
            Thread.Sleep(500);
            Checkbox.Click();
        }

        public void NextButton()
        {
            Thread.Sleep(500);
            NextBtn.Click();
            Thread.Sleep(5000);
        }

        public void EnterKakaoCredentials()
        {
            Thread.Sleep(1000);
            KakaoEmail.SendKeys("rickf@apadmi.com");
            KakaoPassword.SendKeys("ApadmiTesting123");
            KakaoBtn.Click();
            Thread.Sleep(1000);
        }

        public void AuthButton()
        {
            Thread.Sleep(500);
            AuthenticateBtn.Click();
        }

        #endregion

        #region Navigation


        #endregion

        public void TearDown()
        {
            if (_parallelConfig.Driver != null)
            {
                _parallelConfig.Driver.Close();
                //Closes all windows & quits the driver
                _parallelConfig.Driver.Quit();
            }
        }
    }
}
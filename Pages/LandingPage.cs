﻿using NUnit.Framework;
using OpenQA.Selenium;
using QudiniAutomation.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace QudiniAutomation.Pages
{
    public class LandingPage : BasePage
    {
        public LandingPage(ParallelConfig parallelConfig) : base(parallelConfig) { }

        #region UI Elements

        // Landing Page - Language
        private IWebElement LanguageSelect => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[1]/div[1]/div/div/div/div"));
        private IWebElement SelectEnglishLanguage => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='menu-']/div[3]/ul/li[2]"));
        private IWebElement SelectKoreanLanguage => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='menu-']/div[3]/ul/li[1]"));
        // Landing Page - Select Service
        private IWebElement ServiceSelect => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div[2]/div/div/div"));
        private IWebElement Repairs => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='menu-']/div[3]/ul/li[1]"));
        private IWebElement Sales => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='menu-']/div[3]/ul/li[2]"));

        // Landing page - Checkbox & Next button
        private IWebElement Checkbox => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div[4]/div[1]/label/span"));
        private IWebElement NextBtn => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/button"));


        #endregion

        #region Actions

        public void SelectLanguage()
        {
            LanguageSelect.Click();
            Thread.Sleep(500);
        }


        public void SelectEnglish()
        {
            Thread.Sleep(500);
            SelectEnglishLanguage.Click();
            Thread.Sleep(500);
        }


        public void SelectKorean()
        {
            Thread.Sleep(500);
            SelectKoreanLanguage.Click();
            Thread.Sleep(500);
        }

        public void SelectRepairs()
        {
            Thread.Sleep(500);
            ServiceSelect.Click();
            Repairs.Click();
            Thread.Sleep(500);
        }

        public void SelectSales()
        {
            Thread.Sleep(500);
            ServiceSelect.Click(); 
            Sales.Click();
            Thread.Sleep(500);
        }

        public void CheckTerms()
        {
            Thread.Sleep(500);
            Checkbox.Click();
        }

        public void NextButton()
        {
            Thread.Sleep(500);
            NextBtn.Click();
        }

        #endregion

        #region Navigation


        #endregion

        public void TearDown()
        {
            if (_parallelConfig.Driver != null)
            {
                _parallelConfig.Driver.Close();
                //Closes all windows & quits the driver
                _parallelConfig.Driver.Quit();
            }
        }
    }
}

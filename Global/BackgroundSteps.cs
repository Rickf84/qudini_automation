﻿using QudiniAutomation.Base;
using QudiniAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace QudiniAutomation.Global
{
    [Binding]
    public class BackgroundSteps : BaseStep
    {

        #pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        private readonly ParallelConfig _parallelConfig;

        public BackgroundSteps(ParallelConfig parallelConfig) : base(parallelConfig)
        {
            _parallelConfig = parallelConfig;
        }

        [Given(@"I am on the URL with valid token")]
        public void GivenIAmOnTheURLWithValidToken()
        {
            _parallelConfig.Driver.Navigate().GoToUrl("https://chanel-kiosk.netlify.app/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOm51bGwsImV4cCI6MTY2NzM4NDY0MSwiYXVkIjoiIiwic3ViIjoiIiwic3RvcmVJZCI6ImRldiJ9.tTsSNA-OIP49nKtMCvkg8x1KdUYXeisVbkwOex5WB0I");
        }

        [Then(@"The Landing page is displayed")]
        public void ThenTheLandingPageIsDisplayed()
        {
            _parallelConfig.CurrentPage = new LandingPage(_parallelConfig);
        }
    }
}
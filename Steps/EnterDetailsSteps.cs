﻿using NUnit.Framework;
using OpenQA.Selenium;
using QudiniAutomation.Base;
using QudiniAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace QudiniAutomation.Steps
{
    [Binding]
    public sealed class EnterDetailsSteps : BaseStep
    {
        private readonly ParallelConfig _parallelConfig;

        private IWebElement FirstNameValue => _parallelConfig.Driver.FindElement(By.CssSelector("#root > div > div.registration-page-container > div > div.name-fields-container > div:nth-child(1) > div > input"));
        private IWebElement LastNameValue => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[2]/div[2]/div"));
        private IWebElement FirstNameError => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[2]/div[1]"));
        private IWebElement LastNameError => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[2]/div[2]"));
        private IWebElement ErrorMessage => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/p[1]"));
        private IWebElement ErrorInfo => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/p[2]"));
        private IWebElement PhoneNumberError => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[3]/p"));
        private IWebElement DateError => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[5]/div[1]/p"));
        private IWebElement EmailError => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[6]/p"));
        private IWebElement CheckboxError => _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div/div[7]/div[1]/label/span"));


        internal EnterDetailsSteps(ParallelConfig parallelConfig) : base(parallelConfig)
        {
            _parallelConfig = parallelConfig;
        }

        #region GIVEN

        [Given(@"user is on enter details page")]
        public void GivenUserIsOnEnterDetailsPage()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectLanguage();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectEnglish();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectSales();
            _parallelConfig.CurrentPage.As<LandingPage>().NextButton();
            _parallelConfig.CurrentPage = new EnterDetailsPage(_parallelConfig);
        }

        [Given(@"enters valid details")]
        public void GivenEntersValidDetails()
        {
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterFirstName();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterLastName();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterPhoneNumber();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterDoB();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterEmail();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().CheckTerms();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().NextButton();
            Assert.AreEqual("Thank you for waiting.", _parallelConfig.Driver.FindElement(By.XPath("//*[@id='inQueue']/div[1]/h3")).Text);
            //Assert.AreEqual("It's nearly your turn to be seen.", _parallelConfig.Driver.FindElement(By.XPath("//*[@id='calledBackQueue']/div[1]/h3")).Text);
            //Assert.That(_parallelConfig.Driver.FindElement(By.XPath("//*[@id='calledBackQueue']/div[1]/h3"), Is.EqualTo("It's nearly your turn to be seen.")));
        }

        [Given(@"the user tries to enter too many characters in first & last name fields")]
        public void GivenTheUserTriesToEnterTooManyCharactersInFirstLastNameFields()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectLanguage();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectEnglish();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectSales();
            _parallelConfig.CurrentPage.As<LandingPage>().NextButton();
            _parallelConfig.CurrentPage = new EnterDetailsPage(_parallelConfig);
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterLongFirstName();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterLongLastName();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterPhoneNumber();
        }

        [Given(@"the user has entered numbers & special characters")]
        public void GivenTheUserHasEnteredNumbersSpecialCharacters()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectLanguage();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectEnglish();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectSales();
            _parallelConfig.CurrentPage.As<LandingPage>().NextButton();
            _parallelConfig.CurrentPage = new EnterDetailsPage(_parallelConfig);
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterInvalidFirstName();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterInvalidLastName();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterPhoneNumber();
        }

        [Given(@"user tries to access pages outside of correct flow")]
        public void GivenUserTriesToAccessPagesOutsideOfCorrectFlow()
        {
            _parallelConfig.Driver.Navigate().GoToUrl("https://chanel-kiosk.netlify.app/details");
        }

        [Given(@"the user hasn't filled out mandatory fields")]
        public void GivenTheUserHasnTFilledOutMandatoryFields()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectLanguage();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectEnglish();
            _parallelConfig.CurrentPage.As<LandingPage>().SelectSales();
            _parallelConfig.CurrentPage.As<LandingPage>().NextButton();
            _parallelConfig.CurrentPage = new EnterDetailsPage(_parallelConfig);
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().NextButton();
        }

        [Given(@"User has already accepted kakao terms")]
        public void GivenUserHasAlreadyAcceptedKakaoTerms()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().CheckTerms();
            _parallelConfig.CurrentPage.As<LandingPage>().NextButton();
            _parallelConfig.CurrentPage = new EnterDetailsPage(_parallelConfig);
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterKakaoCredentials();
        }

        [Given(@"User fills out remaining details page")]
        public void GivenUserFillsOutRemainingDetailsPage()
        {
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterLastName();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().EnterKakaoPhoneNumber();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().AuthButton();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().CheckTerms();
            _parallelConfig.CurrentPage.As<EnterDetailsPage>().NextButton();
        }

        #endregion

        #region WHEN

        #endregion

        #region THEN

        [Then(@"only the valid amount are input")]
        public void ThenOnlyTheValidAmountAreInput()
        {
            //Assert.AreEqual("Loremipsumdolorsitametconsecte", FirstNameValue.Text);
            //Assert.AreEqual("Loremipsumdolorsitametconsecte", LastNameValue.Text);
            //StringAssert.Contains("Loremipsumdolorsitametconsecte", FirstNameValue.Text);
            Assert.That(FirstNameValue.Text, Is.EqualTo("Loremipsumdolorsitametconsecte"));
        }

        [Then(@"then please enter your name is displayed")]
        public void ThenThenPleaseEnterYourNameIsDisplayed()
        {
            //Assert.AreEqual("FIRST NAME *\r\nPlease enter your first Name", FirstNameError.Text);
            //Assert.AreEqual("LAST NAME *\r\nPlease enter your last name", LastNameError.Text);
            Assert.That(FirstNameError.Text, Is.EqualTo("FIRST NAME *\r\nPlease enter your first Name"));
            Assert.That(LastNameError.Text, Is.EqualTo("LAST NAME *\r\nPlease enter your last name"));
        }

        [Then(@"user is shown the error page")]
        public void ThenUserIsShownTheErrorPage()
        {
            Assert.That(ErrorMessage.Text, Is.EqualTo("OOPS! SOMETHING HAS GONE WRONG"));
            Assert.That(ErrorInfo.Text, Is.EqualTo("PLEASE RE-SCAN QR CODE OR SEEK HELP FROM OUR STAFF."));
        }


        [Then(@"tapping next button shows inline errors")]
        public void ThenTappingNextButtonShowsInlineErrors()
        {
            Assert.That(FirstNameError.Text, Is.EqualTo("FIRST NAME *\r\nPlease enter your first Name"));
            Assert.That(LastNameError.Text, Is.EqualTo("LAST NAME *\r\nPlease enter your last name"));
            Assert.That(PhoneNumberError.Text, Is.EqualTo("Please input a valid phone number"));
            Assert.That(DateError.Text, Is.EqualTo("Please enter valid date"));
            Assert.That(EmailError.Text, Is.EqualTo("Please enter a valid email address"));
            Assert.That(CheckboxError.Displayed);
        }

        [Then(@"User is added to queue")]
        public void ThenUserIsAddedToQueue()
        {
            Assert.AreEqual("Thank you for waiting.", _parallelConfig.Driver.FindElement(By.XPath("//*[@id='inQueue']/div[1]/h3")).Text);
        }

        #endregion
    }
}

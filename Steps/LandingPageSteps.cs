﻿using NUnit.Framework;
using OpenQA.Selenium;
using QudiniAutomation.Base;
using QudiniAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace QudiniAutomation.Steps
{
    [Binding]
    public sealed class LandingPageSteps : BaseStep
    {
        private readonly ParallelConfig _parallelConfig;

        internal LandingPageSteps(ParallelConfig parallelConfig) : base(parallelConfig)
        {
            _parallelConfig = parallelConfig;
        }

        #region Given

        [Given(@"the user is on the landing page")]
        public void GivenTheUserIsOnTheLandingPage()
        {
            _parallelConfig.CurrentPage.As<LandingPage>();
        }

        [Given(@"user selects Sales")]
        public void GivenUserSelectsSales()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectSales();
        }

        [Given(@"user selects Repairs")]
        public void GivenUserSelectsRepairs()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectRepairs();
        }

        #endregion

        #region When

        [When(@"they tap the language dropdown")]
        public void WhenTheyTapTheLanguageDropdown()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectLanguage();
        }

        [When(@"select English")]
        public void WhenSelectEnglish()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectEnglish();
        }

        [When(@"select Korean")]
        public void WhenSelectKorean()
        {
            _parallelConfig.CurrentPage.As<LandingPage>().SelectKorean();
        }

        #endregion

        #region Then

        [Then(@"User is shown the English language")]
        public void ThenUserIsShownTheEnglishLanguage()
        {
            Assert.AreEqual("PLEASE SELECT THE PURPOSE OF YOUR VISIT", _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div[1]/h5[2]")).Text);
        }

        [Then(@"User is shown the Korean language")]
        public void ThenUserIsShownTheKoreanLanguage()
        {
            Assert.AreEqual("PLEASE SELECT THE PURPOSE OF YOUR VISIT - ko", _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div[1]/h5[2]")).Text);
        }

        [Then(@"Sales is shown")]
        public void ThenSalesIsShown()
        {
            Assert.AreEqual("Sales", _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div[2]/div/div/div")).Text);
        }

        [Then(@"Repairs is shown")]
        public void ThenRepairsIsShown()
        {
            Assert.AreEqual("Repairs", _parallelConfig.Driver.FindElement(By.XPath("//*[@id='root']/div/div[2]/div[2]/div/div/div")).Text);
        }

        #endregion

        //// ASSERTION EXAMPLES
        //// Classic model first
        //// Constraint model below - Recommended from Nunit 3

        //[Test(Description = "Simple assertion to check actual & expected are the same")]
        //[Author("Rick", "rickf@apadmi.com")]
        //[Category("Are Equal")]
        //[Order(1)] //Recommended to avoid as tests should not be reliant on previous tests
        //[Platform("Win10")]
        //[Explicit("Reasons")] //Test is ignored but can still be ran on demand
        //[Ignore("Only needs to be ran when required")] //Shows a warning result when run tests
        //public void AreEqual()
        //{
        //    //Assert.AreEqual("expected", "actual");
        //    Assert.That("actual", Is.EqualTo("expected"));
        //}

        //[Test]
        //public void AreNotEqual()
        //{
        //    //Assert.AreNotEqual("expected", "actual");
        //    Assert.That("actual", Is.Not.EqualTo("expected"));
        //}

        //// Constraint Groupings - Is Has Does Contains Throws
        //[Test]
        //public void AreEqualWithDescription()
        //{
        //    Assert.AreEqual("expected", "actual", "what does this even mean");

        //}

        //[Test]
        //public void AreEqualWithDynamicDescription()
        //{
        //    Assert.AreEqual("expected", "actual", "what does {0} mean to {1}", "testing", "you");
        //}

        //[Test]
        //public void AreEqualNumbers()
        //{
        //    Assert.AreEqual(1, 2);
        //}

        //[Test]
        //public void AreEqualWithADifferentType()
        //{
        //    Assert.AreEqual(1, 2d);
        //}

        //[Test]
        //public void AreEqualWithinTolerance()
        //{
        //    Assert.AreEqual(1, 23, .5);
        //}

        //[Test]
        //public void AreEquaArrays()
        //{
        //    var expected = new int[] { 1, 2, 3 };
        //    var actual = new int[] { 1, 3, 2 };
        //    //Array.Sort(actual);
        //    Assert.AreEqual(expected, actual);
        //}

        ////for reference types, even when all properties of an object are the same, they'll not be recognized as equals
        ////because unless it's been overwritten, the equals method does not use value equality, it uses reference equality.
        //[Test]
        //public void AreReferencesEqual()
        //{
        //    var obj1 = new object();
        //    var obj2 = obj1;

        //    Assert.AreSame(obj1, obj2);
        //}
        //[Test]
        //public void ContainsOneInstanceOfThree()
        //{
        //    int[] arrayOfValues = new int[] { 1, 2, 3 };
        //    //Assert.AreEqual(1, arrayOfValues.Count(x => x.Equals(3)));
        //    Assert.That(arrayOfValues, Has.Exactly(1).EqualTo(3));
        //}
        //[Test]
        //public void IsMoreThanFiveAndLessThanHundred()
        //{
        //    var testValue = 100;
        //    Assert.That(testValue, Is.GreaterThan(5).And.LessThan(100));
        //}
        //// Data-driven tests are a nice way to add variation to your test suite, while at the same time, reducing the boilerplate
        //// or repetitive code for cases when you need tests that perform the same actions, just with different datasets.


    }
}

﻿using QudiniAutomation.Base;
using QudiniAutomation.ConfigElement;
using System;


namespace QudiniAutomation.Config
{
    public class ConfigReader
    {
        public static void SetFrameworkSettings()
        {
            Settings.Env = TestConfiguration.Settings.TestSettings["qa"].Env;
            Settings.AUT = TestConfiguration.Settings.TestSettings["qa"].AUT;
            //Settings.BuildName = buildname.Value.ToString();
            Settings.TestType = TestConfiguration.Settings.TestSettings["qa"].TestType;
            Settings.IsLog = TestConfiguration.Settings.TestSettings["qa"].IsLog;
            //Settings.IsReporting = TestConfiguration.Settings.TestSettings["qa"].IsReadOnly;
            Settings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), TestConfiguration.Settings.TestSettings["qa"].Browser);
        }

    }
}
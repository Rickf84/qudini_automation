﻿using System.Configuration;

namespace QudiniAutomation.ConfigElement
{
    public class TestConfiguration : ConfigurationSection
    {
        private static TestConfiguration _testConfig = (TestConfiguration)ConfigurationManager.GetSection("TestConfiguration");

        public static TestConfiguration Settings => _testConfig;

        [ConfigurationProperty("testSettings")]
        public FrameworkElementCollection TestSettings => (FrameworkElementCollection)base["testSettings"];

    }
}

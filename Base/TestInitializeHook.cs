﻿using System;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;
using QudiniAutomation.Helpers;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using OpenQA.Selenium.Safari;
using System.Collections.Generic;
using System.IO;

namespace QudiniAutomation.Base
{
    public class TestInitializeHook
    {

        private readonly ParallelConfig _parallelConfig;

        public TestInitializeHook(ParallelConfig parallelConfig)
        {
            _parallelConfig = parallelConfig;
        }

        [Obsolete]
        public void InitializeSettings()
        {
            //Set all the settings for framework
            //ConfigReader.SetFrameworkSettings();

            //Set Log
            LogHelpers.CreateLogFile();

            //Open Browser
            OpenBrowser(BrowserType.Chrome);

            LogHelpers.Write("Initialized framework");

        }

        [Obsolete]
        private void OpenBrowser(BrowserType browserType = BrowserType.Chrome)
        {
            switch (browserType)
            {
                case BrowserType.FireFox:
                    _parallelConfig.Driver = new FirefoxDriver();
                    _parallelConfig.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    break;
                case BrowserType.Chrome:
                    new DriverManager().SetUpDriver(new ChromeConfig());
                    _parallelConfig.Driver = new ChromeDriver();
                    _parallelConfig.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    break;
                case BrowserType.BsChrome:
                    _parallelConfig.Driver = createBrowserstackChromeDriver();
                    _parallelConfig.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    break;

            }
        }

        [Obsolete]
        public static IWebDriver createBrowserstackChromeDriver()
        {
            string USERNAME = "rickfox1";
            string AUTOMATE_KEY = "SjEvDHeNYMxjD3Sma7fy";

            //ChromeOptions capabilities = new ChromeOptions();

            //Dictionary<string, object> browserstackOptions = new Dictionary<string, object>();

            //browserstackOptions.Add("osVersion", "10.0");
            //browserstackOptions.Add("deviceName", "Samsung Galaxy S20");
            //browserstackOptions.Add("realMobile", "true");
            //browserstackOptions.Add("local", "false");
            //browserstackOptions.Add("userName", "rickfox1");
            //browserstackOptions.Add("accessKey", "SjEvDHeNYMxjD3Sma7fy");
            //capabilities.AddAdditionalOption("bstack:options", browserstackOptions);

            //// For iPhone
            SafariOptions capabilities = new SafariOptions();

            Dictionary<string, object> browserstackOptions = new Dictionary<string, object>();
            browserstackOptions.Add("osVersion", "14");
            browserstackOptions.Add("deviceName", "iPhone 12 Pro Max");
            browserstackOptions.Add("realMobile", "true");
            browserstackOptions.Add("local", "false");
            browserstackOptions.Add("userName", "rickfox1");
            browserstackOptions.Add("accessKey", "SjEvDHeNYMxjD3Sma7fy");
            capabilities.AddAdditionalOption("bstack:options", browserstackOptions);

            IWebDriver driver;
            driver = new RemoteWebDriver(
              new Uri("https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub"), capabilities.ToCapabilities());
            
              return driver;
        }
    }
}
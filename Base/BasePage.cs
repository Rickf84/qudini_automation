﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace QudiniAutomation.Base
{
    public abstract class BasePage : Base
    {
        public BasePage(ParallelConfig parellelConfig) : base(parellelConfig)
        {
        }

        public void MoveToElement(IWebElement element)
        {
            Actions actions = new Actions(_parallelConfig.Driver);
            actions.MoveToElement(element);
            actions.Perform();
        }
    }
}

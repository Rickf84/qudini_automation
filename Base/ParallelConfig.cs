﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;

namespace QudiniAutomation.Base
{
    public class ParallelConfig
    {
        public IWebDriver Driver { get; set; }

        public BasePage CurrentPage { get; set; }

    }
}

﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using OpenQA.Selenium;
using QudiniAutomation.Base;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace QudiniAutomation.Hooks
{

    [Binding]
    public class HookInitialize : TestInitializeHook
    {
        private readonly ParallelConfig _parallelConfig;
        private readonly FeatureContext _featureContext;
        private readonly ScenarioContext _scenarioContext;


        public HookInitialize(ParallelConfig parallelConfig, FeatureContext featureContext, ScenarioContext scenarioContext) : base(parallelConfig)
        {
            _parallelConfig = parallelConfig;
            _featureContext = featureContext;
            _scenarioContext = scenarioContext;
        }



        private static ExtentTest featureName;
        private static ExtentTest scenario;
        private static ExtentReports extent;


        [AfterStep]
        public void AfterEachStep()
        {
            var stepName = _scenarioContext.StepContext.StepInfo.Text;
            var featureName = _featureContext.FeatureInfo.Title;
            var scenarioName = _scenarioContext.ScenarioInfo.Title;
            string screenshotsDirectory = Directory.GetCurrentDirectory() + @"\screenshots";
            var timeString = DateTime.Now.ToString("dd_MM_yy_HH_mm_ss");

            var stepType = _scenarioContext.StepContext.StepInfo.StepDefinitionType.ToString();

            if (_scenarioContext.TestError == null)
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(stepName);
                else if (stepType == "When")
                    scenario.CreateNode<When>(stepName);
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(stepName);
                else if (stepType == "And")
                    scenario.CreateNode<And>(stepName);
            }
            else if (_scenarioContext.TestError != null)
            {
                Console.WriteLine("ERRROOOOORRRRR " + _scenarioContext.TestError.InnerException);
            }

                Screenshot qudiniScreenshot = ((ITakesScreenshot)_parallelConfig.Driver).GetScreenshot();

                if (!Directory.Exists(screenshotsDirectory))
                {
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\screenshots");
                }

                qudiniScreenshot.SaveAsFile(Directory.GetCurrentDirectory() + @"\screenshots\qudiniscreenshot_" + timeString + ".png", ScreenshotImageFormat.Png);
            }

        [BeforeTestRun]
        public static void TestInitalize()
        {
            Console.WriteLine("HELLLO THEREREEEREREREre");
            //InitializeSettings();
            //Settings.ApplicationCon = Settings.ApplicationCon.DBConnect(Settings.AppConnectionString);

            //Initialize Extent report before test starts
            var htmlReporter = new ExtentHtmlReporter(@"C:\extentreport\SeleniumWithSpecflow\SpecflowParallelTest\ExtentReport.html");
            //htmlReporter.Configuration().Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
            //Attach report to reporter
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }


        [BeforeScenario]
        public void Initialize()
        {
            InitializeSettings();
            // Settings.ApplicationCon = Settings.ApplicationCon.DBConnect(Settings.AppConnectionString);

            //Get feature Name
            featureName = extent.CreateTest<Feature>(_featureContext.FeatureInfo.Title);

            //Create dynamic scenario name
            scenario = featureName.CreateNode<Scenario>(_scenarioContext.ScenarioInfo.Title);
        }



        [AfterScenario]
        public void TestStop()
        {
            Console.WriteLine("Quitting Driver");
            //DriverContext.Driver.Quit();
            //Flush report once test completes
            _parallelConfig.Driver.Quit();
        }

        [AfterTestRun]
        public static void TearDownReport()
        {
            //Flush report once test completes
            extent.Flush();

        }
    }
}
